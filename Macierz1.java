// Funkcja sprawdzajaca czy dana macierz jest symetryczna.

import java.util.*;

public class Macierz1 {
	public static void main(String[] args) {
		int w; // liczba wierszy w macierzy
		int k; // liczba kolumn w macierzy
		Scanner scan = new Scanner(System.in);
		System.out.println("Prosze podac liczbe wierszy : ");
		w = scan.nextInt();
		System.out.println("Prosze podac liczbe kolumn : ");
		k = scan.nextInt();
		String t[][] = new String[w][k]; // stworzenie tablicy stringow
		tablica(t,w,k);
		if(check(t,w,k)) 
			System.out.println("Macierz jest symetryczna");
		else
			System.out.println("Macierz nie jest symetryczna");
	}
	
	public static void tablica(String t[][], int w, int k){
		Scanner scan = new Scanner(System.in);
		for(int i=0;i<w;i++) {
			for(int j =0; j<k; j++) {
				System.out.println("Podaj wartosc ["+i+"]["+j+"]"); // wczytanie wartosci do macierzy
				t[i][j]= scan.nextLine();
			}
		}
		System.out.println("Twoja macierz to : "); // Wyswietlenie macierzy
		for(int i = 0; i<w; i++) {
			for (int j = 0; j<k; j++) {
				System.out.format("%5s",t[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n");
	}
	
	public static boolean check(String t[][], int w,int k) {
		if(w!=k) // jesli macierz nie jest kwadratowa to false
			return false;
		for(int i =0; i<w; i++) {
			for(int j =0; j<k; j++ ) {
				if(!(t[i][j].equals(t[j][i]))) // sprawdza czy stringi sa takie same
					return false;
				}
			}
		return true;
	}
}